import React, { useEffect, useState } from "react";
import s from "./Load.module.scss";
import { fadeIn } from "@/constants/variants";
import { motion, useInView } from "framer-motion";
import LoaderVideo from "../LoaderVideo/LoaderVideo";
const Load = () => {
  const [load, setLoad] = useState(true);

  useEffect(() => {
    setTimeout(function () {
      setLoad(false);
    }, 5000);
  }, [load]);

  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(true);
  }, [show]);
  const [isInView, setIsInView] = useState(false);
  const { ref, inView } = useInView({ threshold: 2.4 });

  useEffect(() => {
    if (inView) {
      setIsInView(true);
    }
  }, [inView]);

  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const [style, setStyle] = useState({ opacity: 1 });

  useEffect(() => {
    setTimeout(function () {
      setStyle({ opacity: 0 });
    }, 3000);
  }, [style]);


  return (
    <motion.div
      className={load === true ? s.Load : s.LoadOff}
      variants={isInView && isMounted}
      initial="hidden"
      exit="exit"
      ref={ref}
      animate={{ opacity: isInView && isMounted }}
      style={style}
      transition={{ duration: 4 }}
      visibility={"hidden"}
    >
      <LoaderVideo />
      <motion.div
        className={s.content}
        initial={"hidden"}
        variants={fadeIn("down", 0.4)}
        whileInView={"show"}
        viewport={{ once: true, amount: 0.7 }}
      >
        <img src="/Sf.png" alt="logo" />
        <h1>SNOWFLAKE</h1>
      </motion.div>
    </motion.div>
  );
};

export default Load;
