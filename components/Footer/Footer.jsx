﻿import React from "react";
import s from "./Footer.module.scss";
import { motion } from "framer-motion";

const Footer = () => {
  return (
    <>
      <motion.div
        style={{
          backgroundImage:
            "linear-gradient(0deg, rgba(109, 149, 252, 0.67) -53.12%,  rgba(109, 149, 252, 0) 30.39% )",
        }}
        animate={{
          backgroundImage: [
            "linear-gradient(0deg, rgba(109, 149, 252, 0.67) -10%,  rgba(109, 149, 252, 0) -10% )",
            "linear-gradient(0deg, rgba(109, 149, 252, 0.67) -53.12%,  rgba(109, 149, 252, 0) 40.39% )",
          ],
        }}
        initial="hidden"
        whileInView="visible"
        variants={{
          hidden: {
            background:
              "linear-gradient(0deg, rgba(109, 149, 252, 0.67) -10%,  rgba(109, 149, 252, 0) -10% )",
            opacity: 0,
          },
          visible: {
            background:
              "linear-gradient(0deg, rgba(109, 149, 252, 0.67) -53.12%,  rgba(109, 149, 252, 0) 40.39% )",
            opacity: 1,
          },
        }}
        transition={{ duration: 2.3 }}
        className={s.back}
      >
        <div className={s.footer_container}>
          <motion.h1
            initial="hidden"
            transition={{ duration: 0.6 }}
            whileInView="visible"
            variants={{
              hidden: { scale: 0 },
              visible: { scale: 1 },
            }}
          >
            snowflake
          </motion.h1>
          <motion.p
            initial="hidden"
            transition={{ duration: 0.6 }}
            whileInView="visible"
            variants={{
              hidden: { scale: 0 },
              visible: { scale: 1 },
            }}
          >
            CREATIVE COMPANY
          </motion.p>
          <motion.hr
            initial="hidden"
            transition={{ duration: 0.8 }}
            whileInView="visible"
            variants={{
              hidden: { scale: 0 },
              visible: { scale: 1 },
            }}
          />
          <nav>
            {/* <ul>
                            <a>Categories</a>Ш
                            <a>About</a>
                            <a>Services</a>
                            <a>Portfoao</a>
                            <a>Pages</a>
                            <a>Support</a>
                        </ul> */}
          </nav>
          <motion.p
            initial="hidden"
            transition={{ duration: 0.7 }}
            whileInView="visible"
            variants={{
              hidden: { scale: 0 },
              visible: { scale: 1 },
            }}
          >
            "Технологии что делают бизнес" <br /> "Мы делаем ваш бизнес проще"{" "}
            <br />
            "Сила инноваций в IT компании - <br /> наша главная
            конкурентоспособное преимущество"
            <br /> "Современные технологии что меняют мир"
          </motion.p>
          <motion.nav
            initial="hidden"
            transition={{ duration: 0.7 }}
            whileInView="visible"
            variants={{
              hidden: { scale: 0 },
              visible: { scale: 1 },
            }}
          >
            <a href="">
              <img src="/facebook.png" alt="facebook" />
            </a>
            <a href="">
              <img src="/instagram.png" alt="instagram" />
            </a>
            <a href="">
              <img src="/whatsapp.png" alt="whatsapp" />
            </a>
            <a href="">
              <img src="/linkedin.png" alt="linkedin" />
            </a>
          </motion.nav>
        </div>
      </motion.div>
    </>
  );
};

export default Footer;
